import os
import json

import streamlit as st
from streamlit_file_browser import st_file_browser

from jinja2 import Template

st.set_page_config(layout="wide")
base_path = "/data"
templates_path = "src/templates"
templates_list = os.listdir(templates_path)

#########################################################################
#
# Session Variables
#
#########################################################################

if 'tree_changed' not in st.session_state:
    st.session_state['tree_changed'] = 0

if 'tree_event' not in st.session_state:
    st.session_state['tree_event'] = None

#########################################################################
#
# UI
#
#########################################################################


file_browser_param = {
    "path": base_path, 
    "show_preview": False,
}

st.title("UI Manager")
col1, col2 = st.columns([1, 3])

with col2:
    st.subheader("Create from template")

    temaplete_name = st.selectbox("Select template", templates_list)

    with open(os.path.join(templates_path, temaplete_name), 'r') as f:
        template_string = f.read()
    settings = json.loads(template_string.split('-->')[0][4:].strip())
    template_jinja = Template(template_string)

    file_name = st.text_input("File Name (specify .html)", "test.html")

    st.text('Template Option:')
    params = dict()
    for v in settings['variables']:
        if v['type'] == 'text':
            params[v["name"]] = st.text_input(v['label'], v['default'])
        elif v['type'] == 'long_text':
            params[v["name"]] = st.text_area(v['label'], v['default'])
        elif v['type'] == 'int':
            params[v["name"]] = st.number_input(v['label'], v['default'])

    if st.button("create"):
        rendered_text = template_jinja.render(**params)
        # st.text(rendered_text)
        path = os.path.join(base_path, file_name)
        os.makedirs(os.path.dirname(path), exist_ok=True)
        st.text(path)
        with open(path, 'w') as f:
            f.write(rendered_text)
            st.session_state['tree_changed'] = 1

with col1:
    delete_file = st.button('Delete')
    if delete_file and st.session_state['tree_event']:
        os.remove(os.path.join(base_path, st.session_state['tree_event']['target']['path']))
        st.session_state['tree_changed'] = 2
    
    # trick to redraw
    if st.session_state['tree_changed'] == 1:
        st.session_state['tree_event'] = st_file_browser(**file_browser_param, key='A')
        st.session_state['tree_changed'] = 0
    elif st.session_state['tree_changed'] == 2:
        st.session_state['tree_event'] = st_file_browser(**file_browser_param, key='B')
        st.session_state['tree_changed'] = 0
    else:
        st.session_state['tree_event'] = st_file_browser(**file_browser_param, key='C')
    