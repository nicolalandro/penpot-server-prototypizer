# Penpot Server Prototypizer
This project contains a docker compose with Penpot server and some other services to deploy quick prototypes.

![Usage example](imgs/penpot.gif)

## How to run

```
docker-compose -p penpot up
# bonus for create profiles
# docker exec -ti penpot_penpot-backend_1 ./manage.sh create-profile -u "Your Email" -p "Your Password" -n "Your Full Name"

# penpoat localhost:9001
# html_data static seved localhost:9000
# streamlit prototype serve localhost:9001
```

Clean all:

```
docker-compose rm
docker-compose down -v
```

# References
* [Penpot get started](https://help.penpot.app/technical-guide/getting-started/): for prototype draw
* [nginx](https://www.nginx.com/): is the penpot and static file exposer
* Streamlit: python lib to quick prototype websites
  * [streamlit-file-browser](https://github.com/pragmatic-streamlit/streamlit-file-browser): to have a view of the file sistem
  * Jinja: to use file template with variables and easily replace it
* JS Frontend  
  * js, html: the frontend language
  * [svelte](https://svelte.dev/), [sveltekit](https://kit.svelte.dev/): the frontend frameworks
  * [Svelte Material UI](https://sveltematerialui.com/): for UI component
  * [Nunjucks](https://mozilla.github.io/nunjucks/getting-started.html): for text template