import { readdirSync } from "fs";

const testFolder = '/www/data';

/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    // let ret = []
    // readdir(testFolder);,
    let ret = readdirSync(testFolder).map(x => {return {name: x}});
    console.log(ret);
    return {
        post: ret
    };
}

/** @type {import('./$types').Actions} */
export const actions = {
    default: async (event) => {
        // TODO log the user in
    }
};